package com.hiberForHillel.model;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "task")
public class Task {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "comment")
    private String comment;

    @Column(name = "dead_line")
    private Date dead_line;

    @ManyToOne
    @JoinColumn(name = "status_id")
    private Status status;

    public Task(String name, String description, String comment, Date dead_line, Status status) {
        this.name = name;
        this.description = description;
        this.comment = comment;
        this.dead_line = dead_line;
        this.status = status;
    }

    public Task() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Date getDead_line() {
        return dead_line;
    }

    public void setDead_line(Date dead_line) {
        this.dead_line = dead_line;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Task{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", comment='" + comment + '\'' +
                ", dead_line=" + dead_line +
                ", status=" + status +
                '}';
    }
}
