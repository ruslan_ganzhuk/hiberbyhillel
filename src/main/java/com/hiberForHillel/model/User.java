package com.hiberForHillel.model;

import javax.persistence.*;

@Entity
@Table(name = "user")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(name = "name")
    private String name;
    @Column(name = "surname")
    private String surname;
    @Column(name = "number")
    private String number;
    @Column(name = "email")
    private String email;

    @ManyToOne
    @JoinColumn(name = "role_id",insertable = false, updatable = false)
    private Role role_id;

    @ManyToOne
    @JoinColumn(name = "rating_id",insertable = false, updatable = false)
    private Rating rating_id;

    @Column(name = "specialisation")
    private String specialisation;

    public User(long id,String name, String surname, String number, String email, Role role_id, Rating rating_id, String specialisation) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.number = number;
        this.email = email;
        this.role_id = role_id;
        this.rating_id = rating_id;
        this.specialisation = specialisation;
    }

    public User(String name, String surname, String number, String email, Role role_id, Rating rating_id, String specialisation) {
        this.name = name;
        this.surname = surname;
        this.number = number;
        this.email = email;
        this.role_id = role_id;
        this.rating_id = rating_id;
        this.specialisation = specialisation;
    }

    public User() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Role getRole() {
        return role_id;
    }

    public void setRole(Role role_id) {
        this.role_id = role_id;
    }

    public Rating getRating() {
        return rating_id;
    }

    public void setRating(Rating rating_id) {
        this.rating_id = rating_id;
    }

    public String getSpecialisation() {
        return specialisation;
    }

    public void setSpecialisation(String specialisation) {
        this.specialisation = specialisation;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", number='" + number + '\'' +
                ", email='" + email + '\'' +
                ", role=" + role_id +
                ", rating=" + rating_id +
                ", specialisation='" + specialisation + '\'' +
                '}';
    }
}
