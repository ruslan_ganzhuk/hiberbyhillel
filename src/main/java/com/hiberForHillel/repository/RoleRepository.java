package com.hiberForHillel.repository;

import com.hiberForHillel.configuration.HibernateUtil;
import com.hiberForHillel.model.Role;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;


import java.util.ArrayList;
import java.util.List;

public class RoleRepository {

    public List<Role> getAllRoles(){
        List<Role> roles = new ArrayList<Role>();
        Session session = HibernateUtil.getSession();
        Query query = session.createQuery("FROM Role ");

        roles = query.list();
        session.close();

        return roles;
    }

    public boolean save(Role role){
        Session session = HibernateUtil.getSession();
        try {
            Transaction transaction = session.beginTransaction();
            session.save(role);
            transaction.commit();
            session.close();
            return true;
        }catch (HibernateException ex){
            ex.getMessage();
        }finally {
            session.close();
        }
        return false;
    }

    public boolean remove(Role role){
        try (Session session = HibernateUtil.getSession()){
            Transaction transaction = session.beginTransaction();
            session.remove(role);
            transaction.commit();
            return true;
        }catch (Exception e){
            e.getMessage();
        }
        return false;
    }
}
