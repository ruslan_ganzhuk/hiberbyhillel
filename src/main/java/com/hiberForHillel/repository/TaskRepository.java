package com.hiberForHillel.repository;

import com.hiberForHillel.configuration.HibernateUtil;
import com.hiberForHillel.model.Task;
import org.hibernate.Session;

import org.hibernate.query.Query;
import java.util.ArrayList;
import java.util.List;

public class TaskRepository {

    public List<Task> getAllTasks(){
        List<Task> tasks = new ArrayList<Task>();
        Session session = HibernateUtil.getSession();
        Query query = session.createQuery("FROM Task");
        tasks = query.list();
        session.close();
        return tasks;
    }
}
