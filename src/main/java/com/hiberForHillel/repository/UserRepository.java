package com.hiberForHillel.repository;

import com.hiberForHillel.configuration.HibernateUtil;
import com.hiberForHillel.model.Task;
import com.hiberForHillel.model.User;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Expression;
import org.hibernate.query.Query;
import org.hibernate.Criteria;

import java.util.ArrayList;
import java.util.List;

public class UserRepository {

    public List<User> getAllUsers(){
        List<User> users = new ArrayList<User>();
        Session session = HibernateUtil.getSession();
        Query query = session.createQuery("FROM User");
        users = query.list();
        session.close();
        return users;
    }

    public List<User> getAllUserWithCreteria(){
        List<User> tasks = new ArrayList<User>();
        Session session = HibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        Criteria criteria = session.createCriteria(User.class);
        tasks = criteria.add(Expression.like("name","Rich%"))
                .list();

        return tasks;
    }

    public boolean update(User user){
        User userUpdate = new User();
        Session session;
        try {
            session = HibernateUtil.getSession();
            Transaction transaction = session.beginTransaction();
            userUpdate.setId(user.getId());
            userUpdate.setName(user.getName());
            userUpdate.setSurname(user.getSurname());
            userUpdate.setNumber(user.getNumber());
            userUpdate.setEmail(user.getEmail());
            userUpdate.setRole(user.getRole());
            userUpdate.setRating(user.getRating());
            userUpdate.setSpecialisation(user.getSpecialisation());
            session.update(userUpdate);
            transaction.commit();
            session.close();
            return true;
        }catch (Exception ex){
            ex.getMessage();
        }
        return false;
    }

    public boolean remove(User user){
        try(Session session = HibernateUtil.getSession()) {
            Transaction transaction = session.beginTransaction();
            session.remove(user);
            transaction.commit();
            session.close();
            return true;
        }catch (Exception ex){
            ex.getMessage();
        }
        return false;
    }
}
