package com.hiberForHillel.service;

import com.hiberForHillel.model.Task;
import com.hiberForHillel.repository.TaskRepository;

import java.util.List;

public class TaskService {
    TaskRepository taskRepository = new TaskRepository();

    public List<Task> getAllTasks(){
        return taskRepository.getAllTasks();
    }

}
