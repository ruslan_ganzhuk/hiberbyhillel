package com.hiberForHillel.service;

import com.hiberForHillel.model.Role;
import com.hiberForHillel.repository.RoleRepository;

import java.util.List;

public class RoleService {


    public List<Role> getAllRoles(){
        RoleRepository roleRepository = new RoleRepository();
        return roleRepository.getAllRoles();
    }

    public boolean save(Role role){
        RoleRepository roleRepository = new RoleRepository();
        return roleRepository.save(role);
    }

    public boolean remove(Role role){
        RoleRepository roleRepository = new RoleRepository();
        return roleRepository.remove(role);
    }
}
