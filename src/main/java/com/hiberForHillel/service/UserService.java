package com.hiberForHillel.service;

import com.hiberForHillel.model.User;
import com.hiberForHillel.repository.UserRepository;

import java.util.List;

public class UserService {
    UserRepository userRepository = new UserRepository();

    public List<User> getAllUsers(){
        return userRepository.getAllUsers();
    }

    public List<User> getAllUsersWithCriteria(){
        return userRepository.getAllUserWithCreteria();
    }

    public boolean update(User user){
        return userRepository.update(user);
    }

    public boolean remove(User user){
        return userRepository.remove(user);
    }
}
